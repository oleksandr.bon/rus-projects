
(function ($, Drupal) {
  'use strict';

  /** menu overlay **/
  Drupal.behaviors.menu = {
    attach: function (context, settings) {

      $('.burger', context).once().on('click', function () {
        $(this).toggleClass("active-menu");
        $('.navigation-overlay').toggleClass("active-overlay");
      });

    }
  }

  /** Accordion **/
  Drupal.behaviors.accordion = {
    attach: function (context, settings) {

      $('.paragraph--type--answer-item', context).once().on('click', function () {
        $(this).toggleClass("active-accordeon");
      });

    }
  }



})(jQuery, Drupal);
