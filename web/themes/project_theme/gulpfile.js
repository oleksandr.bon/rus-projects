var gulp = require('gulp'),
  argv = require('yargs').argv,
  autoprefixer = require('gulp-autoprefixer'),
  babel = require('gulp-babel'),
  bs = require('browser-sync').create(),
  del = require('del'),
  imagemin = require('gulp-imagemin'),
  pixrem = require('gulp-pixrem'),
  sass = require('gulp-sass'),
  sassGlob = require('gulp-sass-glob'),
  sourcemaps = require('gulp-sourcemaps'),
  svgmin = require('gulp-svgmin'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  eslint = require('gulp-eslint'),
  scsslint = require('gulp-scss-lint');

var paths = {
  sassSrc: 'sсss/style.scss',
  globalSass: 'sсss/**/*.{scss,sass}',
  sassDest: 'css',
  jsSrc: 'js/*.js',
  jsDest: 'js/build',
  imgSrc: 'images/source/**/*.{png,jpg,gif}',
  imgDest: 'images/optimized',
  svgSrc: 'images/source/**/*.svg',
  svgDest: 'images/optimized',
}

var browserList = [
  'last 1 major version',
  '>= 1%',
  'Chrome >= 45',
  'Firefox >= 38',
  'Edge >= 12',
  'Explorer >= 11',
  'iOS >= 9',
  'Safari >= 9',
  'Android >= 4.4',
  'Opera >= 30'
];

// Clean tasks.
// SASS Compiling Tasks.


gulp.task('sass', function () {
  return gulp.src(paths.sassSrc)
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass({outputStyle: 'compressed', includePaths: ['node_modules/susy/sass']}).on('error', sass.logError))
    // Need the next two lines as an intermediate write, otherwise autoprefizer doesnt cooperate with sourcemaps
    // https://github.com/ByScripts/gulp-sample/blob/master/gulpfile.js
    .pipe(sourcemaps.write({includeContent: false}))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(autoprefixer({overrideBrowserslist: browserList}))
    .pipe(pixrem())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.sassDest))
    .pipe(bs.stream());
});


// Image compression tasks.

gulp.task('imagemin', function() {
  return gulp.src(paths.imgSrc)
    .pipe(imagemin())
    .pipe(gulp.dest(paths.imgDest));
});

gulp.task('svgmin', function() {
  return gulp.src(paths.svgSrc)
    .pipe(svgmin())
    .pipe(gulp.dest(paths.svgDest));
});


// Lint JS and SCSS tasks.

gulp.task('js-lint', function() {
  return gulp.src([paths.jsSrc])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
})

gulp.task('check-jslint', function() {
  return gulp.src([paths.jsSrc])
    .pipe(eslint())
    .pipe(eslint.format());
})

gulp.task('scss-lint', function() {
  return gulp.src('/scss/*.scss')
    .pipe(scsslint({
      'config': './.scss-lint.yml'
    }));
});

// Watch files for change and set Browser Sync.
gulp.task('watcher', function() {
  bs.init({
    files: [
      'css/**/*.css',
      'templates/**/*.twig',
      'images/optimized/**/*.{png,jpg,gif,svg}',
      'js/build/**/*.js',
      '*.theme'
    ],
    proxy: argv.proxy
  });
  gulp.watch(paths.globalSass, gulp.series('sass'));
  gulp.watch(paths.jsSrc, gulp.series('js')).on('change', bs.reload);

});

// Tasks
gulp.task('cleanCSS', function () {
  return del(['./css/**/*']);
});

gulp.task('cleanJS', function () {
  return del(['./js/build/**/*']);
});


// JS tasks.

gulp.task('load-js', function() {
  return gulp.src(paths.jsSrc)
    .pipe(babel({
      presets: [
        ['@babel/preset-env', {
          targets: {
            browsers: browserList
          }
        }]
      ]
    }))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.jsDest));

});

gulp.task('js', gulp.series('cleanJS', 'load-js'));
gulp.task('images', gulp.series('imagemin', 'svgmin'));
gulp.task('checkJS', gulp.series('check-jslint'));
gulp.task('checkCSS', gulp.series('scss-lint'));
gulp.task('watch', gulp.series('js', 'watcher'));
gulp.task('default', gulp.parallel(gulp.series('js'), 'images'));



